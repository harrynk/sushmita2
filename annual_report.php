<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class annual_report extends MX_Controller

{

	function __construct() {

	parent::__construct();

	}


        function index()
        {
              
		//$data['query'] = $this->get('id');
                $data['view_file']= 'front';                              
                $data['annual_report'] = $this->get_annual_report();
//                var_dump($data);die();
//                $this->load->view('front',$data);
		$this->load->module('template');
		$this->template->front($data);

	}
        
         function details(){
            $slug = $this->uri->segment(3);
//            die($slug);
            //$data['query'] = $this->get('id');
//            $details =  $this->get_details_from_slug($slug);
//            $details = $details[0]['slug']; 
//            $data['destinations']=$this->get_destination($slug);
            $data['annual_report']=$this->get_annual_report_category($slug);
            //var_dump($data['success_stories_details']); die;
            //var_dump($data['activities']); die;
            $data['view_file']="details";
            $this->load->module('template');
            $this->template->front($data);
            }
            
          function updates(){
            $slug = $this->uri->segment(3);
//            die($slug);
            //$data['query'] = $this->get('id');
//            $details =  $this->get_details_from_slug($slug);
//            $details = $details[0]['slug']; 
//            $data['destinations']=$this->get_destination($slug);
            $data['annual_report']=$this->get_annual_report_details($slug);
            //var_dump($data['success_stories_details']); die;
            //var_dump($data['activities']); die;
            $data['view_file']="updates";
            $this->load->module('template');
            $this->template->front($data);
            }  
            
            
        function get_annual_report_details($slug){

	$this->load->model('mdl_annual_report');

	$query = $this->mdl_annual_report->get_annual_report_details($slug);

	return $query->result_array();

	} 
           
            
        function get_annual_report_category($slug){

	$this->load->model('mdl_annual_report');

	$query = $this->mdl_annual_report->get_annual_report_category($slug);

	return $query->result_array();

	} 
            
            
        function get_details_from_slug($slug){

	$this->load->model('mdl_annual_report');

	$query = $this->mdl_annual_report->get_details_from_slug($slug);

	return $query->result_array();

	}

        function get_annual_report()
        {	
		$this->load->model('mdl_annual_report');
		$query = $this->mdl_annual_report->get_annual_report();
		$result = $query->result_array();
		return $result;
	}

        function get($order_by){

	$this->load->model('mdl_news_update');

	$query = $this->mdl_news_update->get($order_by);

	return $query;

	}
}